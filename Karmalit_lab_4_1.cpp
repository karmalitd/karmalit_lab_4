#include <conio.h>

#include <time.h>

#include <stdlib.h>

#include <iostream>

#include <stdio.h>

#include <windows.h>
//#include <locale>

using namespace std;    



void gotoxy(int xp, int yp)

{

     COORD new_xy;

     HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

     new_xy.X = xp; new_xy.Y = yp;

     SetConsoleCursorPosition(hStdOut,new_xy);

}



//функція створення файлу та запис матриці

int p1()

{       

     system("cls");

     int i, j, n=0, m=0;

     float x;

     FILE *f;     //f - файлова зміна

     char sf[40];// змінна для повного імені файлу

     cin.ignore(1,'\n');

     printf("Vvedit im'ya pershogo tekstogogo fajlu: ");

     gets(sf);      

     f=fopen(sf, " w+"); //відкриваємо файл для зчитування в текстовому режимі

     if(f == NULL)

     {

               printf("Vinikla pomilka pri stvorenni fajlu \n");

               while (!kbhit());

               return 0;

     }

     printf("Vvedit kilkist ryadkiv matrici: ");

     cin>>n;     

     printf("Vvedit kilkist stovpchikiv matrici: ");

     cin>>m;    

     fprintf(f, "n=%d ", n); // зчитуємо по одному цілі числа, що визначають розмірність матриці

     fprintf(f, "m=%d \n", m);

     for(i=0; i<n; i++)

     {

               for(j=0; j<(m-1); j++)

               {

                        x=(rand()%100-50.0)/5;

                        printf("%7.2f ", x);

                        fprintf(f, "%7.2f ", x);

               }

               x=(rand()%100-50.0)/5;

               printf("%7.2f \n", x);

               fprintf(f, "%7.2f \n", x);

     }

     fclose(f); 

     printf("Stvoreno fajl %s \n",sf);

     while (!kbhit());

     return 0;

}       





//процедура створення та запис у файл символьного рядка

int p2()

{

     system("cls");

     int i, m;

     FILE *f;               // оголошуємо файлову змінну

     char sf[40]; // змінна для повного імені файлу

     char str[77];

     cin.ignore(1,'\n');

     printf("Vvedit im'ya drugogo tekstogogo fajlu: ");

     gets(sf);      

     f=fopen(sf, "w+");//відкриваємо файл для зчитування в текстовому режимі

     if(f == NULL)

     {

               printf("Vinikla pomilka pri stvorenni fajlu \n");

               while (!kbhit());

               return 0;

     }

    

     printf("Dlya pripinennya vvedit porozhni ryadok\n");

     do

     {

               printf("%d: ", i);

               gets(str);

               if (!*str) break;

               m=strlen(str);

               for(i=0;i<m;i++)

                        putc(str[i],f);

               putc('\0',f);

               putc('\n',f);

     }

     while(*str);



     printf("\n"); 

     fclose(f);      // закриваємо файл

     printf("Stvoreno fajl %s \n",sf);

     while (!kbhit());

     return 0;              

}

//процедура створення та запис у файл матриці по елементно

int p3()

{       

     system("cls");

     int i, j, n=0, m=0;

     float x;

     FILE *f;     //f - файлова зміна

     char sf[40];// змінна для повного імені файлу

     cin.ignore(1,'\n');

     printf("Vvedit im'ya binarnogo fajlu: ");

     gets(sf);      

     f=fopen(sf, " w+b"); //відкриваємо файл для роботи з бінарними даними

     if(f == NULL)

     {

               printf("Vinikla pomilka pri stvorenni fajlu \n");

               while (!kbhit());

               return 0;

     }

     printf("Vvedit kilkist ryadkiv matrici: ");

     cin>>n;     

     printf("Vvedit kilkist stovpchikiv matrici: ");

     cin>>m;    

     fwrite(&n, sizeof(int), 1, f); // записуємо першим елементом кількість рядків матриці

     fwrite(&m, sizeof(int), 1, f); // записуємо першим елементом кількість стовпчиків

     for(i=0; i<n; i++)

     {

               for(j=0; j<m; j++)

               {

                        x=(rand()%100-50.0)/5;

                        printf("%7.2f ", x);

                        fwrite(&x, sizeof(float), 1, f);

               }

               cout<<endl;

     }

     fclose(f); 

     printf("Stvoreno fajl %s \n",sf);

     while (!kbhit());

     return 0;

}



//процедура створення та запис у файл матриці по рядках

int p4()

{       

     system("cls");

     int i, j, n=0, m=0;

     float x;

     FILE *f;     //f - файлова зміна

     char sf[40];// змінна для повного імені файлу

     cin.ignore(1,'\n');

     printf("Vvedit im'ya binarnogo fajlu: ");

     gets(sf);      

     f=fopen(sf, " w+b"); //відкриваємо файл для роботи з бінарними даними

     if(f == NULL)

     {

               printf("Vinikla pomilka pri stvorenni fajlu \n");

               while (!kbhit());

               return 0;

     }

     printf("Vvedit kilkist ryadkiv matrici: ");

     cin>>n;     

     printf("Vvedit kilkist stovpchikiv matrici: ");

     cin>>m;    

     float v[m];

     fwrite(&n, sizeof(int), 1, f); // записуємо першим елементом кількість рядків матриці

     fwrite(&m, sizeof(int), 1, f); // записуємо першим елементом кількість стовпчиків

     for(i=0; i<n; i++)

     {

               for(j=0; j<m; j++)

               {

                        x=(rand()%100-50.0)/5;

                        v[j]=x;

                        printf("%7.2f ", x);

               }

               fwrite(&v, sizeof(v), 1, f);

               cout<<endl;

     }

     fclose(f); 

     printf("Stvoreno fajl %s \n",sf);

     while (!kbhit());

     return 0;

}



int main()

{
 system("cls");

 SetConsoleCP(1251); 

 SetConsoleOutputCP(1251);

 srand(time(NULL));

 int p;

 do

 {

   system("cls");

   gotoxy(10,5);

   cout << "          Golovne menyu         ";

   gotoxy(10,7);  cout << " 1. Stvorennya ta zapis u pershij tekstovij fajl matrici";

   gotoxy(10,8);  cout << " 2. Stvorennya ta zapis u drugij tekstovij fajl matrici";

   gotoxy(10,9);  cout << " 3. Stvorennya ta zapis u pershij binarnij fajl matrici";

   gotoxy(10,10); cout << " 4. Stvorennya ta zapis u drugij binarnij fajl matrici";

   gotoxy(10,12); cout << " 0.   Vihid";

   gotoxy(10,13); cout << " Vvedit nomer punktu : ";

   cin >>p;

   switch (p)

   {
	
	case 1:
	
	{
	
	  p1();
	
	  break;
	
	}
	
	
	
	case 2:
	
	{
	
	  p2();
	
	  break;
	
	}
	
	
	
	case 3:
	
	{
	
	  p3();
	
	  break;
	
	}
	
	case 4:
	
	{
	
	  p3();
	
	  break;
	
	}
	
}

 }

 while (p != 0);

 return 0;

}