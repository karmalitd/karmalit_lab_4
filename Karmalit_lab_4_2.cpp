#include <iostream>

#include <stdlib.h>

#include <stdio.h>

#include <windows.h>

using namespace std;    

int main()

{       

         system("cls");

         SetConsoleCP(1251);

         SetConsoleOutputCP(1251);

         int i, j, t=0, n=0, m=0;

         float x;

         FILE *f1, *f2;      //f1 - вхідний файл, *f2 - вихідний файл

         char sf1[40], sf2[40];// змінна для повного імені файлу

         printf("Vvedit im'ya vhidnogo fajlu: ");

         gets(sf1);

         f1=fopen(sf1, "r+");//відкриваємо файл для зчитування в текстовому режимі

         if(f1 == NULL)

         {

                   printf("Vinikla pomilka pri vidkritti fajlu \n");

                   return 0;

         }

         fscanf(f1, "n=%d ", &n); // зчитуємо кількість рядків матриці

         fscanf(f1, "m=%d ", &m); // зчитуємо кількість стовпчиків матриці

         float a[n][m];  

         float r[m];

         float sum=0.0;

         int k;

         for(i=0;i<n;i++)

                   for(j=0;j<m;j++)

                   {

                            fscanf(f1, "%f ", &x);     //зчитуємо елементи матриці

                            a[i][j]=x;

                   }

         for (k = 0; k < (n+m-1); k++){  
			if (k%2==0){
			sum=0;
			if(k<n){   
			i=n-1; 
			j=m-k-1;
			}
			
		   else{
			i=n-(k-m+2); 
			j=0;
			}
		
		   for (; (0<=i)&(j<m);i--,j++){ 
		   sum+=a[i][j];
		   
		   }
			r[t]=sum;
		    //cout<<r[t]<<" ";
			t++;
		
		}
		}

         // виведення на екран матриці та результатів обчислення

         printf("Matricya a[%d,%d]:\n",n,m);

         for(i=0;i<n;i++)

         {

                   for(j=0;j<m;j++)

                            printf("%7.2f ",a[i][j]);

                   printf("\n");

         }

         printf("\n Masiv r(%d): \n",m);

         for (j = 0; j < m; j++)

                   printf("%7.2f ",r[j]);

         printf("\n");

         printf("\nVvedit im'ya fajlu dlya rezultatu: ");

         gets(sf2);    

         f2=freopen(sf2, "w+", stdout); //відкриваємо та перенаправляє потік виведення у заданий файл

         //якщо вдалось відкрити та перенаправити потік виведення усе виведення буде записуватись у заданий файл і не виводитись на екран

         if(f2== NULL)

         {

                   printf("Vinikla pomilka pri vidkritti fajlu \n");

                   return 0;

         }

         printf("Matricya a[%d,%d]:\n",n,m);

         for(i=0;i<n;i++)

         {

                   for(j=0;j<m;j++)

                            printf("%7.2f ",a[i][j]);

                   printf("\n");

         }

         printf("\n Masiv r(%d): \n",m);

         for (j = 0; j < m; j++)

                   printf("%7.2f ",r[j]);

         printf("\n");

         fclose(f2); 

         fclose(f1); 

         return 0;

}       

